resource "aws_s3_bucket_website_configuration" "s3-website-conf" {
  bucket = aws_s3_bucket.bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.s3_website_bucket_name

  force_destroy = true
}